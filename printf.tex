\documentclass[a4,8pt]{article}

\usepackage{minted}
\usepackage{parskip}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}
\usepackage{tikz}

\graphicspath{{./images/}}
\renewcommand{\newline}{\hfill\break}

\begin{document}
\setcounter{secnumdepth}{0}
\pagenumbering{gobble}
\begin{center}
  \section{A Short Guide to \mintinline{c}{printf()} with C/C++}
  \mintinline{c}{printf("format-string"[,arg1,arg2,...,argN]);}
\end{center}

\mintinline{c}{printf}, short for \emph{print formatted}, is used for
`building' a string, often with data stored in variables or available at a
later time, and printing it out according to a format specified in the format
string.

A printf statement consists of the format string, which can contain string
literals and format specifiers, and an optional comma separated list of arguments.

The argument list is only needed if format specifiers are used in the
format string.

\subsection{Format Specifiers}
The syntax for using format specifiers is as follows (square brackets denote optional parameters):\\
\begin{center}
  \texttt{\%{\color{red}[flags]}{\color{blue}[width]}{\color{teal}[.precision]}conversion-character}
\end{center}

\subsection{Flags}
The \texttt{flags} section of a format specifier is used to modify how the
appropriate argument is displayed within the final, formatted string. The
following is a short list with some of the available flags:

\begin{tabular}{r l}
  - :& left-justify (default is to right-justify; useful with a width specification;
  can't be used with 0)\\
  + :& output a plus (+) or minus (-) sign for numerical value\\
  0 :& forces numerical values to be zero-padded (default is no padding;\\
     & useful with a width specification; can't be used with -) \\
  \textvisiblespace\ :& space will display a minus sign if the number is negative,
  or a space if it is positive
\end{tabular}


\subsubsection{Examples}
\begin{center}
  {\small
    \begin{tabular}{|l|c|}
      \hline
      \multicolumn{1}{|c|}{Source code} & \multicolumn{1}{|c|}{Output} \\ \hline
      \mintinline{c}{int foo = -2;} & \multirow{2}{*}{Foo is -2}  \\
      \mintinline{c}{printf("Foo is % d",foo);} & \\ \hline
      \mintinline{c}{int foo = 7;} & \multirow{2}{*}{Foo is +7}  \\
      \mintinline{c}{printf("Foo is %+d",foo);}  & \\ \hline
      \mintinline{c}{int foo = 5;} & \multirow{2}{*}{Foo is \textvisiblespace 5}  \\
      \mintinline{c}{printf("Foo is % d",foo);}  & \\ \hline
    \end{tabular}
  }
\end{center}

\subsection{Width}
The \texttt{width} section of the format specifier is used to create a
sort of `text box' with a minimum size. This is useful if you have a value
that can vary in length and you want to ensure that there is enough room
to display it correctly. Be sure to include space for the decimal point
when deciding how much space to reserve.

\subsubsection{Examples}
\begin{center}
  {\small
    \begin{tabular}{|l|c|}
      \hline
      \multicolumn{1}{|c|}{Source code} & \multicolumn{1}{|c|}{Output} \\ \hline
      \mintinline{c}{int goo = 4;} & \multirow{2}{*}{Goo is \textvisiblespace 4}\\
      \mintinline{c}{printf("Goo is %2d",goo);} & \\ \hline
      \mintinline{c}{int moo = 4;} & \multirow{2}{*}{Moo is 00004}\\
      \mintinline{c}{printf("Moo is %05d",moo);} & \\ \hline

    \end{tabular}
  }
\end{center}

\newline
\newline

\subsection{Conversion-Characters}
The \texttt{conversion-character} section is where you tell \texttt{printf} what
data types you're inserting into the string. The following is a short list
with some of the available conversion characters:

\begin{tabular}{r l}
  d, i :&  for displaying whole numbers, argument is converted to signed decimal notation (byte, short, int)\\
  ld :& for displaying longs\\
  o :& for displaying an unsigned int in octal\\
  u :& for displaying an unsigned int in decimal\\
  x, X :& for displaying an unsigned int in hexadecimal, uses abcdef (X uses ABCDEF)\\
  e, E :& for displaying a double in exponent notation (E uses a capital E for the exponent)\\
  f, F :& display a double in decimal, rounding to \texttt{[precision]} (default precision of 6, 0 treated as 1)\\
  g :& display a double as with f and e, but precision represents significant digits\\
     & (default precision of 6, 0 treated as 1)\\
  c :& for displaying characters\\
  s :& for displaying strings\\
  p :& display the hexadecimal address contained in a pointer\\
  \% :& insert a literal `\%' in to the string
\end{tabular}

\subsubsection{Examples}
\begin{center}
  {\small
    \begin{tabular}{|l|c|}
      \hline
      \multicolumn{1}{|c|}{Source code} & \multicolumn{1}{|c|}{Output} \\
      \hline
      \mintinline{c}{char *str = "Hello";} & \multirow{2}{*}{The address of str is: 0x55a9e9d00004}\\
      \mintinline{c}{printf("The address of str is: %p\n",str);} & \\
        \hline
      \mintinline{c}{char name[] = "Meacham";} & \multirow{2}{*}{First letter of name: `M'}\\
      \mintinline{c}{printf("First letter of name: '%c'",name[0]);} & \\
        \hline
      \mintinline{c}{int a = -559038737;} & \multirow{2}{*}{Your hex is: DEADBEEF}\\
      \mintinline{c}{printf("Your hex is: %X\n",a);} & \\
        \hline
      \mintinline{c}{char name[] = "R. Simms";} & \multirow{2}{*}{Your instructor is: R. Simms}\\
      \mintinline{c}{printf("Your instructor is: %s\n",name);} & \\
        \hline
        \mintinline{c}{printf("Your score on the test is %%82.7\n");} & Your score on the test is \%82.7\\
          \hline
      \end{tabular}
    }
  \end{center}

  \subsection{Precision}
  The \texttt{.precision} section of a format specifier is used to control the
  number of decimal places, or \emph{precision}, displayed for a floating point number. The number is rounded to the specified
  number of decimal places and zero-padded if there is extra space.

  \subsubsection{Examples}
  \begin{center}
    {\small
      \begin{tabular}{|l|c|}
        \hline
        \multicolumn{1}{|c|}{Source code} & \multicolumn{1}{|c|}{Output} \\
        \hline
        \mintinline{c}{double pi = 3.14159;} & \multirow{2}{*}{Pi is 3.142}\\
        \mintinline{c}{printf("Pi is %.3f",pi);} & \\
          \hline
        \mintinline{c}{double triple = 3.12;} & \multirow{2}{*}{Triple is 3.1200}\\
        \mintinline{c}{printf("Triple is %.4f",triple);} & \\
          \hline
        \mintinline{c}{double big_num = 5230000;} & \multirow{2}{*}{big\_num in scientific notation: 5.23e+06}\\
        \mintinline{c}{printf("big_num in scientific notation: %.2e\n",big_num);} & \\
          \hline
      \end{tabular}
    }
  \end{center}

  \subsection{How it knows where to place values}
  When \texttt{printf} `builds' the new string, it replaces the format-specifiers with the arguments it is given, going from left to right:\\
  \hspace*{1.9cm}\begin{tikzpicture}
    \draw[blue,<->] (0,0) .. controls(2.1,1) and (6.8,1) .. (8.7,0);
    \draw[red,<->] (4,0) .. controls(5.35,.7) and (7,.7) .. (9.7,0);
    \draw[orange,<->] (6.5,0) .. controls(7.20,.7) and (9.7,.7) .. (11,0);
  \end{tikzpicture}\\
  \mintinline{c}{printf("%s lives in apartment %d and has $%.2f dollars", name, appt, money);}

    \end{document}

